const AWS = require('aws-sdk')

AWS.config.update({
  region: 'us-west-2',
  endpoint: 'http://localhost:8000'
})

const getOpt = {
  schema: {
    response: {
      200: {
        type: 'object',
        properties: {
          _id: { type: 'string' },
          name: { type: 'string' },
          status: { type: 'string' },
          created_at: { type: 'string' },
          updated_at: { type: 'string' }
        }
      }
    }
  }
}
const getOpts = {
  schema: {
    response: {
      200: {
        type: 'array',
        items: {
          type: 'object',
          properties: {
            _id: { type: 'string' },
            name: { type: 'string' },
            status: { type: 'string' },
            created_at: { type: 'string' },
            updated_at: { type: 'string' }
          }
        }
      }
    }
  }
}
const postOpt = {
  schema: {
    body: {
      type: 'object',
      required: ['year', 'title'],
      properties: {
        year: { type: 'number' },
        title: { type: 'string' },
        info: {
          type: 'object',
          properties: {
            plot: { type: 'string' },
            rating: { type: 'number' },
            actors: { type: 'array' }
          }
        }
      }
    }
  }
}

const docClient = new AWS.DynamoDB.DocumentClient()

function routes (fastify, options, done) {
  // API TO INSERT DATA INTO MOVIES
  fastify.post('/insert', postOpt, async (request, reply) => {
    const year = request.body.year
    const title = request.body.title
    const info = request.body.info
    const sp = {}
    console.log('the year', year)
    console.log('the title', title)
    console.log('the info', info)
    // query
    const table = 'Movies'
    const params = {
      TableName: table,
      Item: {
        year: year,
        title: title,
        info: info,
        sp: sp
      }
    }
    docClient.put(params).promise()
    return JSON.stringify({ message: 'Data inserted into Table' })
  })
  // API TO GET DATA OF SPECIFIC YEAR
  fastify.get('/year', getOpt, async (request, reply) => {
    const table = 'Movies'

    const year = 2021
    const params = {
      TableName: table,
      KeyConditionExpression: '#yr = :yyyy',
      ExpressionAttributeNames: {
        '#yr': 'year'
      },
      ExpressionAttributeValues: {
        ':yyyy': year
      }
    }
    return docClient.query(params).promise()
  })

  // API TO GET ALL  DATA
  fastify.get('/all', getOpts, async (request, reply) => {
    const params = {
      TableName: 'Movies',
      ProjectionExpression: '#yr, title,info,id',
      ExpressionAttributeNames: {
        '#yr': 'year'
      }
    }
    return docClient.scan(params).promise()
  })

  // API TO GET SPECIFIC TTILE with query
  fastify.get('/title', async (request, reply) => {
    const table = 'Movies'
    const title = 'Fault in Out Stars'
    const year = 2014
    const params = {
      TableName: table,
      KeyConditionExpression: '#yr = :yyyy and title = :tttt',
      ExpressionAttributeNames: {
        '#yr': 'year'
      },
      ExpressionAttributeValues: {
        ':yyyy': year,
        ':tttt': title
      }

    }
    const at = docClient.query(params).promise()
    return at
  })

  // API TO GET SPECIFIC TTILE  with get method
  fastify.get('/', async (request, reply) => {
    console.log('get all the rec')

    const table = 'Movies'

    const year = 2015
    const title = 'The Big New Movie'

    const params = {
      TableName: table,
      Key: {
        year: year,
        title: title
      }
    }

    docClient.get(params, function (err, data) {
      if (err) {
        console.error('Unable to read item. Error JSON:', JSON.stringify(err, null, 2))
      } else {
        console.log('GetItem succeeded:', JSON.stringify(data, null, 2))
      }
    })
    return docClient.get(params).promise()
  })

  done()
}

module.exports = routes
