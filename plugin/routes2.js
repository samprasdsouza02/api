
const AWS = require('aws-sdk')

AWS.config.update({
  region: 'us-west-2',
  endpoint: 'http://localhost:8000'
})

const docClient = new AWS.DynamoDB.DocumentClient()

function routes2 (fastify, request, done) {
  // create API to Insert into the Grocery table
  fastify.post('/insert', async (request, reply) => {
    const productId = request.body.productId
    const productName = request.body.productName
    const productUnitPrice = request.body.productUnitPrice
    const totalUnits = request.body.totalUnits
    const transaction = request.body.transaction
    const remaining = totalUnits
    const customers = {}
    console.log(transaction)
    console.log(productName)
    const table = 'onlineGroceryStore'
    const params = {
      TableName: table,
      Item: {
        ProductId: productId,
        ProductName: productName,
        ProductUnitPrice: productUnitPrice,
        TotalUnits: totalUnits,
        Remaining: remaining,
        Transaction: transaction,
        Customers: customers
      }
    }
    docClient.put(params).promise()
    return JSON.stringify({ message: 'Data Inserted into OnlineGroceryStore Table' })
  })

  // API TO GET ALL THE DATA FROM
  fastify.get('/all', async (request, reply) => {
    const table = 'onlineGroceryStore'
    const params = {
      TableName: table,
      ProjectionExpression: '#PId,ProductName,ProductUnitPrice,TotalUnits,Remaining,Customers,#Tran',
      ExpressionAttributeNames: {
        '#PId': 'ProductId',
        '#Tran': 'Transaction'
      }
    }
    return docClient.scan(params).promise()
  })

  // API WHEN CUSTOMER WANTS TO BUY A PRODUCT (UPDATE QUERY)
  fastify.post('/buy', async (request, reply) => {
    const table = 'onlineGroceryStore'

    const ProductId = request.body.ProductId
    const customerInfo = request.body.customerInfo
    const Quantity = request.body.customerInfo.Quantity

    const params = {
      TableName: table,
      KeyConditionExpression: '#pid = :pid',
      ExpressionAttributeNames: {
        '#pid': 'ProductId'
      },
      ExpressionAttributeValues: {
        ':pid': ProductId
      }
    }

    let ProductObject
    let check = 0
    await docClient.query(params, CheckIfCustomerCanBuy).promise()
    function CheckIfCustomerCanBuy (err, data) {
      if (err) {
        console.error('Unable to scan the table. Error JSON:', JSON.stringify(err, null, 2))
      } else {
        data.Items.forEach(function (itemdata) {
          ProductObject = itemdata
        })
        const remaining = ProductObject.Remaining
        const productId = ProductObject.ProductId
        // update query
        if (Quantity <= remaining) {
          const remainingUnits = remaining - Quantity
          const updtCustomer = ProductObject.Customers
          const nxtTransaction = ProductObject.Transaction + 1
          const currTransaction = 'Transaction-' + ProductObject.Transaction
          updtCustomer[currTransaction] = customerInfo
          const params = {
            TableName: table,
            Key: {
              ProductId: productId
            },
            UpdateExpression: 'set Customers = :c ,Remaining =:r, #Tran =:t',
            ExpressionAttributeNames: {
              '#Tran': 'Transaction'
            },
            ExpressionAttributeValues: {
              ':r': remainingUnits,
              ':c': updtCustomer,
              ':t': nxtTransaction
            }
          }
          docClient.update(params).promise()
        } else {
          check = 1
        }
      }
    }
    if (check === 1) return { Result: ' Not Executed ' }
    else return { Result: 'Query  Sucessfully' }
  })

  // API TO GET PRODUCTS  BY ID
  fastify.get('/:ProductId', async (request, reply) => {
    const productId = parseInt(request.params.ProductId)
    const table = 'onlineGroceryStore'
    console.log(productId)
    const params = {
      TableName: table,
      KeyConditionExpression: '#id =:pid',
      ExpressionAttributeNames: {
        '#id': 'ProductId'
      },
      ExpressionAttributeValues: {
        ':pid': productId
      }
    }
    return docClient.query(params).promise()
  })

  // API TO UPDATE  THE  REMAINING  PRODUCTS
  fastify.post('/updateQuantity', async (request, reply) => {
    const ProductId = request.body.productId
    const updtRemain = request.body.updateRemain
    const table = 'onlineGroceryStore'
    let ProductObject
    const params = {
      TableName: table,
      KeyConditionExpression: '#pid = :pid',
      ExpressionAttributeNames: {
        '#pid': 'ProductId'
      },
      ExpressionAttributeValues: {
        ':pid': ProductId
      }
    }

    return docClient.query(params, UpdateQuantity).promise()

    function UpdateQuantity (err, data) {
      if (err) {
        console.error('Unable to scan the table. Error JSON:', JSON.stringify(err, null, 2))
      } else {
        data.Items.forEach(function (itemdata) {
          ProductObject = itemdata
        })
        console.log(ProductObject)
        const newTotalQuantity = ProductObject.TotalUnits + updtRemain
        const newRemain = ProductObject.Remaining + updtRemain

        const params = {
          TableName: table,
          Key: {
            ProductId: ProductId
          },
          UpdateExpression: 'set Remaining = :r, TotalUnits=:tu',
          ExpressionAttributeValues: {
            ':r': newRemain,
            ':tu': newTotalQuantity
          }
        }
        docClient.update(params).promise()
      }
    }
  })

  // API DELETE A PRODUCT BY GIVEN ID

  fastify.delete('/deleteProduct', async (request, reply) => {
    const ProductId = request.body.productId
    const table = 'onlineGroceryStore'

    const params = {
      TableName: table,
      Key: {
        ProductId: ProductId
      },
      ConditionExpression: 'ProductId = :val',
      ExpressionAttributeValues: {
        ':val': ProductId
      }
    }
    return docClient.delete(params).promise()
  })

  done()
}

module.exports = routes2
