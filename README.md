## BASIC CRUD OPERATIONS
  CRUD API using node.js, fastify and dynamoDB (Local)
### SETUP
 
 INSTALL DOCKER AND DYNAMODB LOCAL
 [DYNAMODB LOCAL ON DOCKER](https://hub.docker.com/r/amazon/dynamodb-local)


> docker-compose up

Create Movies Table
>node MoviesCreateTable.js

Create Online Grocery Store Table
>node groceryStoreCreateTable.js

> npm install

>npm start